// eslint-disable-next-line no-unused-vars
function includeHTML() {
    var z, i, elmnt, file;
    /*loop through a collection of all HTML elements:*/
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
      elmnt = z[i];
      file = elmnt.getAttribute("w3-include-html");
      //alert(file);
      if (file) {
        fetch(file)
        .then(function(response) {
          if (!response.ok) {throw new Error(`HTTP error! status: ${response.status}`);}
          return response.text();
        })
        .then(function(text) {
          elmnt.innerHTML = text;
        })
        .catch(function(err) {
          elmnt.innerHTML = 'Failed to load content. Error: ' + err;
        })
        .finally(function () {
          elmnt.removeAttribute("w3-include-html");
          includeHTML();
        });
        return;
      }
    }
}