import {request} from '/controller/requestAj.js';

export const examinationController = {
    add: async function (examination) {
        await request('examination', 'ADD', examination);
    },
    updateByID: async function (examination) {
        await request('examination', 'UPDATE', examination);
    },
    deleteByID: async function (examination) {
        await request('examination', 'DELETE', examination);
    }
}