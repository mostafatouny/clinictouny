export function isStringLength(str_in, len) {
    if (str_in.length == len) {return true;}
    return false;
}

export function isStrAllInt(str_in) {
    for (let i = 0; i < str_in.length; i++) {
        const el = str_in[i];
        if (isNaN( parseInt(el) )) {
            return false;
        }
    }
    return true;
}

///

export async function patientNumberCount(phone_in) {
    let responseText = fetch("/database/dbAj.php?table=patient&operation=CHECK&phone_in="+phone_in)
        .then(function(response) {
            if (!response.ok) {throw `HTTP error! status: ${response.status}`;}
            return response.text();
        })
        .then (function(responseText) {
            return responseText;
        });
    return await responseText;
}

///

export function nameIn_valid(name_in) {
    if (name_in.length == 0) {throw 'length0Msg'}
    if (name_in.length > 100) {throw 'lengthGreater100Msg';}
}

export function birthdate_valid (birthdate_in) {
    if (birthdate_in.length == 0) {throw 'emptyBirthdate';}
}

export function phoneIn_valid (phone_in, responseMsg_in) {
    return new Promise(function(resolve, reject) {
        phone_in = phone_in.replaceAll(' ', '');

        if (!isStringLength(phone_in, 11)) {reject('Length11Msg'); return;}
        if (!isStrAllInt(phone_in)) {reject('NumbersOnlyMsg'); return;}

        const pnc = patientNumberCount(phone_in)
        .then(function(responseText) {
            if (responseText in responseMsg_in) {
                reject(responseMsg_in[responseText]);
            }
        })
        .catch(function(err) {throw err;});

        Promise.all([pnc]).then(() => {resolve();});
    });
}