import {request} from '/controller/requestAj.js';

export const patientController = {
    add: async function (patient) {
        await request('patient', 'ADD', patient);
    }
}