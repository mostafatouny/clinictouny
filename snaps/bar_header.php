<?php
require '../CONTENT/contentObjByLangP.php';
$contObj = getContentObjByLangP('BAR_HEADER');

$title = $contObj->titleHeader;
$registration = $contObj->registrationMenu;
$retrieval = $contObj->retrievalMenu;
$langValOrder = $contObj->langValOrder;
$langLabOrder = $contObj->langLabOrder;
$logout = $contObj->logout;

echo "
    <header>
        <a id=title> $title </a>
    </header>
    <div>
        <div id='leftDiv'>
            <select name='langSel' id='langSelId' onchange='updateLang()'><option value= $langValOrder[0] > $langLabOrder[0] </option><option value= $langValOrder[1] > $langLabOrder[1] </option></select>
            <form action='/sys/sysLoginSession.php'> <input type='submit' value='$logout'> <input type='hidden' name='operation' value='DELETE'> </form>
        </div>
        <div class='nav'>
        <span><a href='/pages/registration/registration.php'> $registration </a></span>
        <span><a href='/pages/retrieval/retrieval.php'> $retrieval </a></span>
        </div>
    </div>
    ";
?>