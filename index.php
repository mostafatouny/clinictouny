<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'sys/sysAuth.php'; authIfNot();
session_start();

require 'CONTENT/const.php';
if ( !isset($_SESSION['lang']) ) { $_SESSION['lang'] = language::ENGLISH; }
?>

<html>

<body>
    <script>
        window.location.replace("/pages/retrieval/retrieval.php");
    </script>
</body>

</html>