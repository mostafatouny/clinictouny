<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require_once 'database/sql_class.php';

class patient {
    private $ID;
    private $name;
    private $birthdate;
    private $phoneNumber;


    public function setID($ID_in) {$this->ID = $ID_in;}
    public function getID() {return $this->ID;}
    public function setName($name_in) {$this->name = $name_in;}
    public function getName() {return $this->name;}
    public function setBirthdate($birthdate_in) {$this->birthdate = $birthdate_in;}
    public function getBirthdate() {return $this->birthdate;}
    public function setPhoneNumber($phoneNumber_in) {$this->phoneNumber = $phoneNumber_in;}
    public function getPhoneNumber() {return $this->phoneNumber;}


    public function getPatientByNumber() {
        $phoneNumber = $this->getPhoneNumber();

        if (is_null($phoneNumber)) {
            error_log('a NULL phone number is needed for getPatientByNumber in db_class.php');
            throw 'No phone number is given to fetch the patient';
        }

        $getSql = new getSql('patient', array('phone_number'=>$phoneNumber));
        $patient_row = $getSql->getResult()[0];

        $this->setID($patient_row[0]);
        $this->setName($patient_row[1]);
        $this->setBirthdate($patient_row[2]);
        $this->setPhoneNumber($patient_row[3]);
    }
    public function checkPatientExistsByNumber() {
        $phoneNumber = $this->getPhoneNumber();
        if (is_null($phoneNumber)) {
            error_log('a NULL phone number is needed for checkPatientExistsByNumber in db_class.php');
            throw 'No phone number is given to check the patient';
        }

        $getSql = new getSql('patient', array('phone_number'=>$phoneNumber));
        $rows = $getSql->getResult();
        $rows_length = count($rows);

        if ($rows_length == 0) {return 0;}
        else if ($rows_length == 1) {return 1;}
        return 2;
    }
    public function addNewPatient() {
        $name = $this->getName();
        $phone = $this->getPhoneNumber();
        $birthdate = $this->getBirthdate();

        $insertSql = new insertSql('patient', array('name'=>$name, 'phone_number'=>$phone, 'birthdate'=>$birthdate));
    }
    public function deletePatientByNumber() {
        $phoneNumber = $this->getPhoneNumber();
        $deleteSql = new deleteSql('patient', array('phone_number'=>$phoneNumber) );
    }

    public function __construct($phoneNumber_in=NULL, $name_in=NULL, $birthdate_in=NULL, $ID_in=NULL) {
        $this->setID($ID_in);
        $this->setName($name_in);
        $this->setBirthdate($birthdate_in);
        $this->setPhoneNumber($phoneNumber_in);
    }
}
?>