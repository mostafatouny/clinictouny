<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require_once 'database/sql_class.php';

class examination {
    private $ID;
    private $content;
    private $date;

    public function setID($ID_in) {$this->ID = $ID_in;}
    public function getID() {return $this->ID;}
    public function setContent($content_in){$this->content = $content_in;}
    public function getContent(){return $this->content;}
    public function setDate($date_in) {$this->date = $date_in;}
    public function getDate() {return $this->date;}

    public function __construct($ID_in, $content_in, $date_in) {
        $this->setID($ID_in);
        $this->setContent($content_in);
        $this->setDate($date_in);
    }
}

class examinationList {
    private $examinationList;


    public function getExaminationList() {
        return $this->examinationList;
    }
    public function addExaminationToList($ID_in, $content_in, $date_in) {
        $examination = new examination($ID_in, $content_in, $date_in);
        array_push($this->examinationList, $examination);
    }
    public function getDatedExamination($date_in) {
        for ($i=0; $i < $this->examinationList.length(); $i++) {
            if ($examinationList[$i].getDate() == $date_in) {return $examinationList[$i];}
        }
    }



    public function getExaminationListByPatientID($patientID_in) {
        $patientID = $patientID_in;

        $getSql = new getSql('examination', array('patient_fk'=>$patientID), 'ORDER BY date ASC');

        $rows = $getSql->getResult();
        foreach ($rows as $row) {
            $this->addExaminationToList($row[0], $row[1], $row[3]);
        }

        return $this->getExaminationList();
    }
    public function addExamination($content_in, $patientID_in, $date_in) {
        $content = $content_in;
        $patientFK = $patientID_in;
        $date = $date_in;

        $insertSql = new insertSql('examination', array('content'=>$content, 'patient_fk'=>$patientFK, 'date'=>$date));
    }
    public function updateExaminationByID($examinationID_in, $content_in) {
        $examinationID = $examinationID_in;
        $content = $content_in;
        $updateSql = new updateSql('examination', array('content'=>$content), array('ID'=>$examinationID));
    }
    public function deleteExaminationByID($examinationID_in) {
        $ID = $examinationID_in;
        $deleteSql = new deleteSql('examination', array('ID'=>$ID));
    }



    public function __construct() {
        $this->examinationList = array();
    }
}
?>