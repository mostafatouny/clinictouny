<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'database/patient.php';
require 'database/examination.php';

$table = $_GET['table'];
$operation = $_GET['operation'];

switch ($table) {
    case "patient":

        switch ($operation) {
            case 'ADD':
                $patientStr = $_GET['patientStr'];
                $patient = json_decode($patientStr);

                $name = $patient->name;
                $phoneNumber = $patient->phone;
                $birthdate = $patient->birthdate;

                try {
                    $patient = new patient($phoneNumber, $name, $birthdate);
                    $patient->addNewPatient();
                } catch (\Exception $exc) {
                    header('HTTP/ 500 '.$exc->getMessage(), true, 500); // 500 internal server error
                        // status=500, statusText=$exc->getMessage()
                        // both data can be fetched from client javascript's response object
                }
                break;

            case 'CHECK':
                $phoneNumber = $_GET['phone_in'];
                $patient = new patient($phoneNumber);
                echo $patient->checkPatientExistsByNumber();

            default:
                break;
        }

    break;


    case "examination":

        switch ($operation) {
            case 'UPDATE':
                $examinationStr = $_GET['examinationStr'];
                $examination = json_decode($examinationStr);

                $ID = $examination->ID;
                $content = $examination->content;

                try {
                    $examinationList = new examinationList();
                    $examinationList->updateExaminationByID($ID, $content);
                } catch (\Exception $exc) {
                    header('HTTP/ 500 '.$exc->getMessage(), true, 500); // 500 internal server error
                        // status=500, statusText=$exc->getMessage()
                        // both data can be fetched from client javascript's response object
                }
                break;

            case 'DELETE':
                $examinationStr = $_GET['examinationStr'];
                $examination = json_decode($examinationStr);

                $ID = $examination->ID;

                try {
                    $examinationList = new examinationList();
                    $examinationList->deleteExaminationByID($ID);
                } catch (\Exception $exc) {
                    header('HTTP/ 500 '.$exc->getMessage(), true, 500); // 500 internal server error
                        // status=500, statusText=$exc->getMessage()
                        // both data can be fetched from client javascript's response object
                }
                break;

            case 'ADD':
                $examinationStr = $_GET['examinationStr'];
                $examination = json_decode($examinationStr);

                $content = $examination->content;
                $patientFK = $examination->patientFK;
                $date = $examination->date;

                try {
                    $examinationList = new examinationList();
                    $examinationList->addExamination($content, $patientFK, $date);
                } catch (\Exception $exc) {
                    header('HTTP/ 500 '.$exc->getMessage(), true, 500); // 500 internal server error
                        // status=500, statusText=$exc->getMessage()
                        // both data can be fetched from client javascript's response object
                }
                break;

            default:
                break;
        }

    break;

    default:
        break;
}
?>