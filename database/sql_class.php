<?php

function whereSqlParser($WHERE_in) {
    $sql = 'WHERE ';
    foreach ($WHERE_in as $key => $value) {
        $sql = $sql . "$key='$value' AND ";
    }
    $sql = substr($sql, 0, -4);

    return $sql;
}

function setSqlParser($SET_in) {
    $sql = 'SET ';
    foreach ($SET_in as $key => $value) {
        $sql = $sql . "$key='$value', ";
    }
    $sql = substr($sql, 0, -2);
    $sql = $sql . ' ';

    return $sql;
}

///

class sql {
    protected $sql;
    protected $result;

    public function setSql($sql_in) {$this->sql = $sql_in;}
    public function getSql() {return $this->sql;}
    public function setResult($result_in) {$this->result = $result_in;}

    public function execute() {
        chdir($_SERVER['DOCUMENT_ROOT']);
        //require 'database/db_auth.php';

        //$conn = new mysqli($servername, $username, $password, $dbname); // fetched from db_auth.php

        // local db
        $servername = "localhost";
        $username = "test_user";
        $password = "235791";
        $dbname = "clinic_db";

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {throw new Exception("Connection failed: " . $conn->connect_error);}
        $result = $conn->query($this->getSql());
        if ($result == false) {throw new Exception("Error: " . $sql . "<br>" . $conn->error);}
        return $result;
    }
}

///

class getSql extends sql {
    public function getResult() {
        $rowsList = array();
        while ($row = $this->result->fetch_row()) {array_push($rowsList, $row);}
        return $rowsList;
    }

    public function sqlToStr($table_in, $WHERE_in, $plus_in='') {
        $sql = "SELECT * FROM $table_in ";

        // WHERE
        $sql = $sql . whereSqlParser($WHERE_in);
        $this->setResult($sql);

        // plus
        $sql = $sql . ' ' . $plus_in;

        return $sql;
    }

    public function __construct($table_in, $WHERE_in, $plus_in='') {
        $sql = $this->sqlToStr($table_in, $WHERE_in, $plus_in='');
        $this->setSql($sql);
        $result = $this->execute();
        $this->setResult($result);
    }
}

class insertSql extends sql {

    public function sqlToStr($table_in, $VALUES_in, $plus_in='') {
        $sql = "INSERT INTO $table_in (";

        // VALUES_in
        foreach ($VALUES_in as $key => $value) {
            $sql = $sql . "$key, ";
        }
        $sql = substr($sql, 0, -2);
        $sql = $sql . ") VALUES (";
        foreach ($VALUES_in as $key => $value) {
            $sql = $sql . "'$value', ";
        }
        $sql = substr($sql, 0, -2);
        $sql = $sql . ')';

        return $sql;
    }

    public function __construct($table_in, $VALUES_in, $plus_in='') {
        $sql = $this->sqlToStr($table_in, $VALUES_in, $plus_in='');
        $this->setSql($sql);
        $this->execute();
    }
}

class updateSql extends sql {

    public function sqlToStr($table_in, $SET_in, $WHERE_in, $plus_in='') {
        $sql = "UPDATE $table_in ";

        $sql = $sql . setSqlParser($SET_in);
        $sql = $sql . whereSqlParser($WHERE_in);

        return $sql;
    }

    public function __construct($table_in, $SET_in, $WHERE_in, $plus_in='') {
        $sql = $this->sqlToStr($table_in, $SET_in, $WHERE_in, $plus_in='');
        $this->setSql($sql);
        $this->execute();
    }
}


class deleteSql extends sql {

    public function sqlToStr($table_in, $WHERE_in, $plus_in='') {
        $sql = "DELETE FROM $table_in ";
        $sql = $sql . whereSqlParser($WHERE_in);
        return $sql;
    }

    public function __construct($table_in, $WHERE_in, $plus_in='') {
        $sql = $this->sqlToStr($table_in, $WHERE_in, $plus_in='');
        $this->setSql($sql);
        $this->execute();
    }

}
?>
