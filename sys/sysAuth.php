<?php

function authIfNot () {
    session_start();
    chdir($_SERVER['DOCUMENT_ROOT']);
    require 'sys/SYSUSERS.php';

    $user = $_SESSION['username'];
    $pass = $_SESSION['password'];

    $validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

    if (!$validated) { header("Location: /sys/sysLogin.php"); }
}

?>