<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'CONTENT/contentObjByLangP.php';

$contObj = getContentObjByLangP('SYSLOGIN');
$nameLabel = $contObj->userNameLabel;
$passwordLabel = $contObj->passwordLabel;
$currentlyIncorrect = $contObj->currentlyIncorrect;
$registerData = $contObj->registerData;
?>


<html>

<script src="/scripts/includeHTML.js"></script>
<script src='/CONTENT/reverse_ar.js'></script>

<head>
    <title>Sys Login - Dr. X</title>
    <link rel="stylesheet" href="/styles.css">
    <meta charset="UTF-8">
</head>


<body>
    <div id="outerFrame">

        <div w3-include-html="/snaps/bar_header.php" class='bar'></div>

        <main>
            <p class='error' id='errorMsg'><?php echo $currentlyIncorrect ?></p>
            <form action='/sys/sysLoginSession.php'>
                <table>
                    <tr class='reversible'>
                        <th><?php echo $nameLabel ?></th>
                        <td><input type='text' name='username' style='width:50%'></td>
                    </tr>
                    <tr class='reversible'>
                        <th><?php echo $passwordLabel ?></th>
                        <td><input type='password' name='password' style='width:50%'></td>
                    </tr>
                </table>
                <input type='submit' value='<?php echo $registerData ?>'>
                <input type='hidden' name='operation' value='UPDATE'>
            </form>
        </main>

        <footer w3-include-html="/snaps/footer.php"></footer>

    </div>

    <script>includeHTML(); reverse_ar(["1"]);</script>
                            <!-- alt: mark elements by includeHTML's pattern -->
    <!-- further: script included in html snippet -->
    <script src='/CONTENT/langUpdater.js'></script>
</body>

</html>