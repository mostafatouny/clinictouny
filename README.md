### CRUD Website
- Data includes users and their list of long-text
- Create, Read, Update, Delete
- MVC Architecture
  - Model: _database_
  - View: _pages_
  - Controller: _controller_
- Vanilla MySql, HTML, CSS, Javascript, PHP
- Display in English and Arabic languages
- Validation of entered phone number
- Deployed on [Heroku](https://clinictouny.herokuapp.com/), but disabled to save resources

### Demo
Click [here](https://odysee.com/@mostafatouny:7/clinictouny:7)
