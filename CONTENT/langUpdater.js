// eslint-disable-next-line no-unused-vars
function updateLang() {
    const langId = document.getElementById('langSelId').value;

    fetch("/CONTENT/langAj.php?operation=UPDATE&lang_in="+langId)
    .then(function(response) {
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
        location.reload();
    })
    .catch(function(error) {
        console.log(error.message);
    });
}