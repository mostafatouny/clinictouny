export function getStrByLang(fileStr_in, labelStr_in) {
    return new Promise(function(resolve, reject) {
        fetch("/CONTENT/contentObjByLangAj.php?fileStr="+fileStr_in)
        .then(function(response) {
            if (!response.ok) {throw new Error(`HTTP error! status: ${response.status}`);}
            return response.text();
        })
        .then(function(responseText) {
            const contObj = JSON.parse(responseText);
            resolve(contObj[labelStr_in]);
        });
    });
}