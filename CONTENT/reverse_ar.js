function reverseDates() {
    let dates = document.getElementsByClassName('date');
    for (var i=0; i<dates.length; i++) {
        let date = dates[i].innerText;
        dates[i].innerText = ((date.split('-')).reverse()).join(' - ');
    }
}

function datesToAr() {
    let dates = document.getElementsByClassName('date');
    for (var i=0; i<dates.length; i++) {
        let date = dates[i].innerText;
        for (var j=48; j<=57; j++) {
            date = date.replaceAll(String.fromCharCode(j), String.fromCharCode(j+1584));
        }
        dates[i].innerText = date;
    }
}

///

function reverseTextArea() {
    let els = document.getElementsByTagName('textarea');
    for (var i=0; i<els.length; i++) {
        els[i].setAttribute('dir', 'rtl');
    }
}

///

function reverseList(li) {
    for (var i = li.length; i--;) {
        li[i].parentNode.appendChild(li[i]);
        // The same node cannot exist in multiple positions
        // It's removed from its current position and placed at the end
    }
}

function reverseLists(liFamily) {
    liFamily = document.getElementsByClassName('reversible');
    for (var i=0; i<liFamily.length; i++) {
        reverseList(liFamily[i].children); //get HTML collection of children
    }
}

///

// liFamily is HTML collection
// eslint-disable-next-line no-unused-vars
function reverse_ar(langList) {
    fetch("/CONTENT/langAj.php?operation=GET")
    .then(function(response) {
        if (!response.ok) {throw new Error(`HTTP error! status: ${response.status}`);}
        return response.text();
    })
    .then(function(responseText) {
        if (langList.includes(responseText)) {
            reverseLists();
            reverseTextArea();
            datesToAr();
        }
    })
    .catch(function(err) {
      alert(err + '\nPlease contact the developer');
    })
    .finally(()=> {
      reverseDates();
    });
}