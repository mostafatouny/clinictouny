<?php
function getContentObjByLang($fileStr_in) {
    // change directory to project's root (inside clinic folder) -> CONTENT
    chdir($_SERVER['DOCUMENT_ROOT']);
    //chdir('CONTENT');

    session_start();
    require 'const.php';
    $lang = $_SESSION['lang'];

    $langStr = $lang==language::ENGLISH ? 'ENGLISH' : 'ARABIC';
    $fileStr = $fileStr_in;

    $contStr = file_get_contents('CONTENT/CONTENT.json');
    $contObj = json_decode($contStr, false);

    return $contObj->$langStr->$fileStr;
}
?>