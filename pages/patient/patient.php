<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'sys/sysAuth.php'; authIfNot();
require 'CONTENT/contentObjByLangP.php';
require 'database/patient.php';

$contObj = getContentObjByLangP('PATIENT');

$nameLabel = $contObj->nameTable;
$birthdate = $contObj->birthTable;
$phoneNumber = $contObj->phoneTable;
$examination = $contObj->examination;

$patient_number = $_GET['phone_number'];

$patient = new patient($patient_number);
$patient->getPatientByNumber();

$patientID = $patient->getID();
$patientName = $patient->getName();
$patientBirthdate = $patient->getBirthdate();
$patientNumber = $patient->getPhoneNumber();
?>


<html>

<script src="/scripts/includeHTML.js"></script>
<script src='/CONTENT/reverse_ar.js'></script>

<head>
    <title>Patient - Dr. X</title>
    <link rel="stylesheet" href="/styles.css">
    <meta charset="UTF-8">
</head>


<body>
    <div id="outerFrame">

        <div w3-include-html="/snaps/bar_header.php" class='bar'></div>

        <main>
            <table>
                <tr class='reversible'>
                    <th><?php echo $nameLabel ?></th>
                    <td><?php echo $patient->getName(); ?></td>
                </tr>
                <tr class='reversible'>
                    <th><?php echo $birthdate ?></th>
                    <td><?php echo $patient->getBirthdate(); ?></td>
                </tr>
                <tr class='reversible'>
                    <th><?php echo $phoneNumber ?></th>
                    <td><?php echo $patient->getPhoneNumber(); ?></td>
                </tr>
            </table>
            <form action='/pages/examination/examination.php'>
                <input type='submit' value='<?php echo $examination ?>'></input>
                <input type='hidden' value='<?php echo $patient->getID() ?>' name='ID'>
            </form>
        </main>

        <footer w3-include-html="/snaps/footer.php"></footer>

    </div>

    <script>includeHTML(); reverse_ar(["1"]);</script>
                            <!-- alt: mark elements by includeHTML's pattern -->
    <!-- further: script included in html snippet -->
    <script src='/CONTENT/langUpdater.js'></script>
</body>

</html>