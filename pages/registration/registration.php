<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'CONTENT/contentObjByLangP.php';
require 'sys/sysAuth.php'; authIfNot();

$contObj = getContentObjByLangP('REGISTRATION');

$nameLabel = $contObj->nameLabel;
$phoneLabel = $contObj->phoneLabel;
$birthLabel = $contObj->birthLabel;
$submitLabel = $contObj->submitLabel;
?>


<html>


<script src='/scripts/includeHTML.js'></script>
<script src='/CONTENT/reverse_ar.js'></script>


<head>
    <title>Registration - Dr. X</title>
    <link rel="stylesheet" href="/styles.css">
    <meta charset="UTF-8">
</head>


<body>
    <div id="outerFrame">

        <div w3-include-html="/snaps/bar_header.php" class='bar'></div>

        <main>
            <p class='error' id='errorMsg'></p>
            <p class='success' id='successMsg'></p>
            <form action='#' id='registrationForm'>
                <div id='nameEntry' class='reversible'>
                    <label> <?php echo $nameLabel ?> </label><input type='text' id ='name_in'>
                </div>
                <div id='phoneEntry' class='reversible'>
                    <label> <?php echo $phoneLabel ?> </label><input type='text' id ='phone_in'>
                </div>
                <div id='birthdateEntry' class='reversible'>
                    <label> <?php echo $birthLabel ?> </label><input type='date' id ='birthdate_in'>
                </div>
                <input type='submit' value=<?php echo $submitLabel ?> id='submitButton'>
            </form>
        </main>

        <footer w3-include-html="/snaps/footer.php"></footer>

    </div>

    <script>includeHTML(); reverse_ar(["1"]);</script>
                            <!-- alt: mark elements by includeHTML's pattern -->
    <!-- further: script included in html snippet -->
    <script src='/CONTENT/langUpdater.js'></script>

    <script type='module' src='/pages/registration/registration.js'></script>
</body>

</html>