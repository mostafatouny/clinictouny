import {msgHead} from '/scripts/message.js';
import {getStrByLang} from '/CONTENT/strByLang.js';
import {nameIn_valid, birthdate_valid, phoneIn_valid} from '/controller/validation.js';
import {patientController} from '/controller/patient.js';


async function valid(name_in, phone_in, birthdate_in) {
    // sync function with exceptions
    nameIn_valid(name_in); // Its exception is passed here, and caught by async .catch
                            // remaining code isn't called
                           // If no exception, code continues

    birthdate_valid(birthdate_in);

    // async promise with with rejections
    const responseMsg = {
        '1':'patientExistsMsg',
        '2':'notUniqueMsg'
    }
    await phoneIn_valid(phone_in, responseMsg); // Its rejection is passed here, and caught by async .catch
                                    // remaining code isn't called
                                   // if resolves, code continues

    // if no exception or rejection is triggered, it resolves
}

const submissionBehavior = function (event) {
    // prevent form from default submitting
    event.preventDefault();

    document.body.style.cursor = 'progress';

    msgHead(''); // clean previous error message
    msgHead('', 1); // clean previous success message

    const name = document.getElementById('name_in').value;
    const phone = document.getElementById('phone_in').value;
    const birthdate = document.getElementById('birthdate_in').value; // year-month-day

    valid(name, phone, birthdate)
    .then (()=> {
        const patient = {"name":name, "phone":phone, "birthdate":birthdate};
        return patient;
    })
    //.then ( addPatient )
    .then ( patientController.add )
    .then (()=> {getStrByLang('REGISTRATION', 'success').then((value)=> {msgHead(value, 1);});})
    .catch((errorMsg)=> {getStrByLang('REGISTRATION', errorMsg).then((value)=> {msgHead(value);});})
    .finally (()=> {
        document.body.style.cursor = 'default';
    });
}

// define submission behavior on both click and enter events
document.getElementById('submitButton').onclick = submissionBehavior;
document.getElementById('registrationForm').onkeydown = function (event) {
    if (event.code === 'Enter') {submissionBehavior(event);}
}