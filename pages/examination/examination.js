import {msgHead} from '/scripts/message.js';
import {examinationController} from '/controller/examination.js';

function getCurrentSlide () {
    var slides = document.getElementsByClassName("mySlides");
    for (var i = 0; i < slides.length; i++) {
        if (slides[i].style.display == "block") {
           return slides[i];
        }
    }
}

function getCurrentDateStr () {
    const dateObj = new Date();
    return dateObj.toISOString().slice(0,10);
}

///

async function submitAddExamination() {
    const content = document.getElementById('addExamination').value;
    const patientFK = document.getElementById('patientFK').value;
    const dateStr = getCurrentDateStr();

    const examination = {'content':content, 'patientFK':patientFK, 'date':dateStr};

    await examinationController.add(examination);
}

async function updateExamination() {
    const currSlide = getCurrentSlide();

    const ID = currSlide.getElementsByTagName('input')[0].value;
    const content = currSlide.getElementsByTagName('textarea')[0].value;

    const examination = {'ID': ID, 'content': content}

    await examinationController.updateByID(examination);
}

async function deleteExamination() {
    const currSlide = getCurrentSlide();

    const ID = currSlide.getElementsByTagName('input')[0].value;

    const examination = {'ID': ID};

    await examinationController.deleteByID(examination);
}

///

document.getElementById('addButton').onclick = function () {
    document.body.style.cursor = 'progress';

    submitAddExamination()
    .then( ()=> {
        location.reload()
    })
    .catch(function (errorMsg) {
        msgHead(errorMsg);
    })
    .finally (function () {
        document.body.style.cursor = 'default';
    });
}

document.getElementById('updateButton').onclick = ()=> {
    document.body.style.cursor = 'progress';

    updateExamination()
    .then( ()=> {
        location.reload()
    })
    .catch(function (errorMsg) {
        msgHead(errorMsg);
    })
    .finally (function () {
        document.body.style.cursor = 'default';
    });
}

document.getElementById('deleteButton').onclick = ()=> {
    document.body.style.cursor = 'progress';

    deleteExamination()
    .then( ()=> {
        location.reload()
    })
    .catch(function (errorMsg) {
        msgHead(errorMsg);
    })
    .finally (function () {
        document.body.style.cursor = 'default';
    });
}
