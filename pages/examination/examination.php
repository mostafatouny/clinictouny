<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'CONTENT/contentObjByLangP.php';
require 'database/examination.php';
require 'sys/sysAuth.php'; authIfNot();

$ID = $_GET['ID'];

$examinationListObj = new examinationList();
$examinationList = $examinationListObj->getExaminationListByPatientID($ID);

$contObj = getContentObjByLangP('EXAMINATION');

$nextLabel = $contObj->nextLabel;
$prevLabel = $contObj->prevLabel;
$updateLabel = $contObj->modifyLabel;
$deleteLabel = $contObj->deleteLabel;
$addLabel = $contObj->addLabel;
$submitLabel = $contObj->submitLabel;
?>


<html>

<script src='/scripts/includeHTML.js'></script>
<script src='/CONTENT/reverse_ar.js'></script>


<head>
    <title>Examination - Dr. X</title>
    <link rel="stylesheet" href="/styles.css">
    <meta charset="UTF-8">
</head>


<body>
    <div id="outerFrame">

        <div w3-include-html="/snaps/bar_header.php" class='bar'></div>

        <main>
            <input type='hidden' id='patientFK' value='<?php echo $ID ?>'>
            <p class='error' id='errorMsg'></p>
            <p class='success' id='successMsg'></p>

            <div class='slideshow-container'>
                <?php
                    foreach ($examinationList as $examination) {
                        echo "<div class='mySlides' style='margin:0'>";
                        echo "<span class='date'>" . $examination->getDate() . "</span>";
                        echo "<br>";
                        echo "<textarea rows='5' cols='45'>";
                        echo $examination->getContent();
                        echo "</textarea>";
                        echo "<input type='hidden' value='".$examination->getID()."'>";
                        echo "</div>";
                    }
                ?>
                <span class='reversible'>
                    <button id='updateButton'><?php echo $updateLabel?></button>
                    <button id='deleteButton'><?php echo $deleteLabel?></button>
                    <button class="prev" onclick="plusSlides(-1)"><?php echo $prevLabel?></button>
                    <button class="next" onclick="plusSlides(1)"><?php echo $nextLabel?></button>
                </span>
            </div>
            <details>
                <summary style='cursor:pointer'> <?php echo $addLabel?> </summary>
                <textarea rows=5 cols=45 id='addExamination'></textarea>
                <button id='addButton'><?php echo $submitLabel?></button>
            </details>
        </main>

        <footer w3-include-html="/snaps/footer.php"></footer>

    </div>

    <script>includeHTML(); reverse_ar(["1"]);</script>
                            <!-- alt: mark elements by includeHTML's pattern -->
    <!-- further: script included in html snippet -->
    <script src='/CONTENT/langUpdater.js'></script>
    <script src='/pages/examination/slideshow.js'></script>
    <script type='module' src='/pages/examination/examination.js'></script>

</body>

</html>