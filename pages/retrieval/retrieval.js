import {msgHead} from '/scripts/message.js';
import {getStrByLang} from '/CONTENT/strByLang.js';
import {phoneIn_valid} from '/controller/validation.js';


const submissionBehavior = function (event) {
    // prevent form from default submitting
    event.preventDefault();

    document.body.style.cursor = 'progress';

    const form = document.getElementById('phoneForm');
    const phone = document.getElementById('phone_in').value;

    const responseMsg = {
        '0':'noPatientMsg',
        '2':'notUniqueMsg'
    }

    phoneIn_valid(phone, responseMsg)
    .then(function () {
        form.submit();
    }).catch(function (errorMsg) {
        getStrByLang('RETRIEVAL', errorMsg).then(function (value) {msgHead(value);});
    }).finally (function () {
        document.body.style.cursor = 'default';
    });
}

// define submission behavior on both click and enter events
document.getElementById('submitButton').onclick = submissionBehavior;
document.getElementsByTagName('form')[0].onkeydown = function (event) {
    if (event.code === 'Enter') {submissionBehavior(event);}
}