<?php
chdir($_SERVER['DOCUMENT_ROOT']);
require 'sys/sysAuth.php'; authIfNot();
require 'CONTENT/contentObjByLangP.php';
$contObj = getContentObjByLangP('RETRIEVAL');

$submit = $contObj->submitLabel;
$phoneNumber = $contObj->phoneLabel;
?>


<html>


<script src='/scripts/includeHTML.js'></script>
<script src='/CONTENT/reverse_ar.js'></script>


<head>
    <title>Retrieval - Dr. X</title>
    <link rel="stylesheet" href="/styles.css">
    <meta charset="UTF-8">
</head>


<body>
    <div id="outerFrame">

        <div w3-include-html="/snaps/bar_header.php" class='bar'></div>

        <main>
            <p class='error' id='errorMsg'></p>
            <p class='success' id='successMsg'></p>
            <form action='/pages/patient/patient.php' method='get' id='phoneForm' class='reversible'>
            <label> <?php echo $phoneNumber ?> </label><input type='text' name='phone_number' id ='phone_in'>
                <input type='submit' value='<?php echo $submit ?>' id='submitButton'>
            </form>
        </main>

        <footer w3-include-html="/snaps/footer.php"></footer>

    </div>

    <script>includeHTML(); reverse_ar(["1"]);</script>
                            <!-- Family of lists, as a HTML collection. innerly a list's children are expanded -->
    <!-- further: script included in html snippet -->
    <script src='/CONTENT/langUpdater.js'></script>

    <script type='module' src='retrieval.js'></script>
</body>

</html>